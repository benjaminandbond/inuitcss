# SASS Framework

This is the baseline SASS repository for our projects using inuit.css. This project was constructed using 'yarn' for dependency management and webpack for handling the build process. All dependencies and files needed are listed below.

## Dependencies
* autoprefixer
* css-loader
* node-sass
* postcss-loader
* sass-loader
* style-loader
* webpack

## postcss.config.js
```javascript
module.exports = {
  plugins: [
    require('autoprefixer')
  ]
}
```

## webpack.config.js
```javascript
const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    app: './scripts/app.js',
  },
  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ]
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].bundle.js',
  },
};
```